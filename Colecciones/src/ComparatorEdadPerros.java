import java.util.Comparator;


public class ComparatorEdadPerros implements Comparator<Perro> {

    @Override
    public int compare(Perro o1, Perro o2) {
        return o1.getEdad().compareTo(o2.getEdad());
    }

}
