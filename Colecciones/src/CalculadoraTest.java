import org.junit.Test;
import static org.junit.Assert.*;


public class CalculadoraTest {

    @Test
    public void deberiaDarCeroSiDividendoEsCeroYDivisorDistintoCero() {
        // preparamos contexto
        Calculadora casio = new Calculadora();
        // ejecutamos la funcionalidad
        double resultado = casio.dividir(0, 4);
        // validamos lo esperado con lo obtenido
        assertEquals(0.0, resultado, 0.01);
    }
    
    @Test
    public void deberiaDividirSiDividendoYDivisorDistintoCero() {
        // preparamos contexto
        Calculadora casio = new Calculadora();
        // ejecutamos la funcionalidad
        double resultado = casio.dividir(8, 4);
        // validamos lo esperado con lo obtenido
        assertEquals(2.0, resultado, 0.01);
    }

}
