import java.util.HashSet;
import java.util.TreeSet;

import org.junit.Test;


public class MiPrueba {

    @Test
    public void test() {
        HashSet<Persona> set = new HashSet<Persona>();
        set.add(new Persona(12,"seba"));
        set.add(new Persona(12,"seba"));
        set.add(new Persona(12,"sebas"));
        System.out.println(set);
        
        TreeSet<Perro> tree = new TreeSet<Perro>();
        tree.add(new Perro(12,"seba"));
        tree.add(new Perro(10,"ana"));
        tree.add(new Perro(15,"maria"));
        tree.add(new Perro(9,"maria"));
        System.out.println(tree);
        
        tree = new TreeSet<Perro>(new ComparatorEdadPerros());
        tree.add(new Perro(12,"seba"));
        tree.add(new Perro(10,"ana"));
        tree.add(new Perro(15,"maria"));
        tree.add(new Perro(9,"maria"));
        System.out.println(tree);
        
    }

}
